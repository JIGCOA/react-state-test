import React, { Component } from "react";
import Kill from "./Kill";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Kill />
        </header>
      </div>
    );
  }
}

export default App;
