import React, { Component } from "react";

class Defencer extends Component {
  onFieldChange(event) {
    const fieldName = event.target.name;
    const fieldValue = event.target.value;
    this.props.handleAllstate(fieldName, fieldValue);
  }
  render() {
    return (
      <div className="Defencer">
        <div className="">
          <label className="" /> HP
          <input
            className="form-control "
            type="text"
            name="Hp"
            onChange={this.onFieldChange.bind(this)}
          />
          <label className="" /> Defence
          <input
            className="form-control"
            type="text"
            name="Defence"
            onChange={this.onFieldChange.bind(this)}
          />
        </div>
      </div>
    );
  }
}

export default Defencer;
