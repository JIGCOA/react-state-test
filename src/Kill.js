import React, { Component } from "react";
import Attack from "./Attack";
import Defencer from "./Defencer";

class Kill extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AttackPower: "",
      AttackSpeed: "",
      CriticalChance: "",
      CriticalMultiplier: "",
      Hp: "",
      Defence: "",

      HitDamage: 0,
      DPS: 0,
      ImpactDamage: 0,
      TimeToKill: 0
    };
    this.handleAllstate = this.handleAllstate.bind(this);
    this.AttackKill = this.AttackKill.bind(this);
  }
  handleAllstate(field, value) {
    this.setState({
      //[e.target.name]: e.target.value
      [field]: value
    });
  }
  HitDamage(vAttackPower, vCriticalChance, vCriticalMultiplier) {
    return new Promise(function(res, rej) {
      if (
        vAttackPower != null &&
        vCriticalChance != null &&
        vCriticalMultiplier != null
      ) {
        const valueAttackPower = parseFloat(
          vAttackPower +
            vAttackPower * (vCriticalMultiplier - 1) * (vCriticalChance / 100)
        ).toFixed(2);
        console.log(valueAttackPower);
        res(valueAttackPower);
      } else {
        rej("Error");
      }
    });
  }
  DPS(Hid, vAttackSpeed) {
    return new Promise(function(res, rej) {
      if (Hid != null && vAttackSpeed != null) {
        const valueDPS = parseFloat(Hid * vAttackSpeed).toFixed(2);
        console.log(valueDPS);
        res(valueDPS);
      } else {
        rej("Error");
      }
    });
  }
  ImpactDamage(Hid, vDefence, vAttackSpeed) {
    return new Promise(function(res, rej) {
      if (Hid != null && vDefence != null && vAttackSpeed != null) {
        const valueImpactDamage = parseFloat(
          (Hid - vDefence) * vAttackSpeed
        ).toFixed(2);
        console.log(valueImpactDamage);
        res(valueImpactDamage);
      } else {
        rej("Error");
      }
    });
  }
  TimeToKill(Im, vhp) {
    return new Promise(function(res, rej) {
      if (Im != null && vhp != null) {
        const valueTimeToKill = parseFloat(vhp / Im).toFixed(2);
        console.log(valueTimeToKill);
        res(valueTimeToKill);
      } else {
        rej("Error");
      }
    });
  }
  async AttackKill() {
    if (
      this.state.AttackPower != "" &&
      this.state.AttackSpeed != "" &&
      this.state.CriticalChance != "" &&
      this.state.CriticalMultiplier != "" &&
      this.state.Hp != "" &&
      this.state.Defence != ""
    ) {
      const vAttackPower = parseInt(this.state.AttackPower);
      const vAttackSpeed = parseInt(this.state.AttackSpeed);
      const vCriticalChance = parseInt(this.state.CriticalChance);
      const vCriticalMultiplier = parseFloat(this.state.CriticalMultiplier);
      const vhp = parseInt(this.state.Hp);
      const vDefence = parseInt(this.state.Defence);

      try {
        const Hid = await this.HitDamage(
          vAttackPower,
          vCriticalChance,
          vCriticalMultiplier
        );
        const [Dps, Im] = await Promise.all([
          this.DPS(Hid, vAttackSpeed),
          this.ImpactDamage(Hid, vDefence, vAttackSpeed)
        ]);
        const Ttk = await this.TimeToKill(Im, vhp);
        this.setState({
          HitDamage: Hid,
          DPS: Dps,
          ImpactDamage: Im,
          TimeToKill: Ttk
        });
      } catch (rej) {
        console.log("Error " + rej);
      }
    } else {
      alert("Please fill in all information");
    }
  }

  componentDidUpdate() {
    console.log(this.state);
  }
  render() {
    return (
      <div>
        <div className="container mt-4 ">
          <div className="row">
            <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Attacker Attribute</h5>
                  <Attack handleAllstate={this.handleAllstate} />
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Defencer </h5>
                  <Defencer handleAllstate={this.handleAllstate} />
                </div>
              </div>
            </div>
          </div>

          <div className="row mt-4">
            <div className="col-sm-12 col-md-10 col-lg-12 col-xl-12">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Attacker Damage output</h5>
                  <p>
                    Damage per hit {parseFloat(this.state.HitDamage).toFixed(1)}
                  </p>
                  <p>DPS {parseFloat(this.state.DPS).toFixed(1)}</p>
                  <h5 className="card-title">Time to Kill</h5>
                  <p>{this.state.TimeToKill} Second</p>
                  <button className="btn btn-primary" onClick={this.AttackKill}>
                    Test Attack
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Kill;
