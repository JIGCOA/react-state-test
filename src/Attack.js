import React, { Component } from "react";

class Attack extends React.Component {
  onFieldChange(event) {
    const fieldName = event.target.name;
    const fieldValue = event.target.value;
    this.props.handleAllstate(fieldName, fieldValue);
  }
  render() {
    return (
      <div className="Attack">
        <div class="">
          <label className="" /> AttackPower
          <input
            className="form-control  "
            type="text"
            name="AttackPower"
            onChange={this.onFieldChange.bind(this)}
          />
          <label className="" /> AttackSpeed
          <input
            className="form-control "
            type="text"
            name="AttackSpeed"
            onChange={this.onFieldChange.bind(this)}
          />
          <label className="" /> CriticalChance
          <input
            className="form-control  "
            type="text"
            name="CriticalChance"
            onChange={this.onFieldChange.bind(this)}
          />
          <label className="" /> CriticalMultiplier
          <input
            className="form-control  "
            type="text"
            name="CriticalMultiplier"
            onChange={this.onFieldChange.bind(this)}
          />
        </div>
      </div>
    );
  }
}

export default Attack;
